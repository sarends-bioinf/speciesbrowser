package nl.bioinf.sarends.species_browser.dao;


import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;

import java.io.IOException;

public class UserDaoFactory {

    private static UserInterface daoInstance;

    public static void initializeDataSource(String type) throws DataBaseException {
        if (daoInstance != null){
            throw new IllegalStateException("DAO can be initialized only once");
        }
        switch (type){
            case "mysql":{
                createUserInstance();
                break;
            }
            default: throw new IllegalArgumentException("unknown database type requested");
        }
    }

    /**
     * serves the dao instance
     * @return
     */
    public static UserInterface getDataSource() {
        if (daoInstance == null) {
            throw new IllegalStateException("DAO is not initialized; call initializeDataSource() first");
        }
        return daoInstance;
    }

    public static void createUserInstance() throws DataBaseException{
        try {
            DbUser dbUser = DbCredentials.getMySQLuser();
            String user = dbUser.getUserName();
            String passWrd = dbUser.getDatabasePassword();
            String host = dbUser.getHost();
            String dbName = dbUser.getDatabaseName();
            String dbUrl = "jdbc:mysql://" + host + "/" + dbName;
            daoInstance = MyDbConnector.getInstance(dbUrl, user, passWrd);
            daoInstance.connect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }


    }

}
