package nl.bioinf.sarends.species_browser.model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvParser {

    private String filePath;
    private List<String> data = new ArrayList<>();

    public CsvParser() {
    }

    public CsvParser(String filePath) {
        this.filePath = filePath;
    }

    public static void main(String[] args) throws FileNotFoundException {
        CsvParser csvParser = new CsvParser();
        csvParser.createAnimalList(csvParser.parseFile("/homes/sarends/jaar_3/Thema_10/WIS/speciesbrowser/data/species.csv"));
    }

    public List<String> parseFile(String filePath) throws FileNotFoundException {
        try {
            List<String> instances = new ArrayList<>();

            File inFile = new File(filePath);

            BufferedReader breader = null;

            breader = new BufferedReader(new FileReader(inFile));

            String sCurrentline;
            while ((sCurrentline = breader.readLine()) != null) {
                if (sCurrentline.startsWith("scientific_name")){continue;}
                else {
                    instances.add(sCurrentline);
                }
            }

            return instances;

        } catch (FileNotFoundException e) {
            System.err.println("[CsvParser] It looks like that the file cannot be opened. Please check if the file is not corrupted.");
            //throw new FileNotFoundException("Something went wrong" + e);
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }

    public List<Animal> createAnimalList(List<String> instances){
        List<Animal> animalsList = new ArrayList<>();
        for (int i = 0; i < instances.size(); i++){
            //System.out.println(instances.get(i));
            String[] animalObjects = instances.get(i).split(";");
            Animal animals = new Animal(animalObjects[0],animalObjects[1], animalObjects[2], Double.parseDouble(animalObjects[3]), animalObjects[4]);
            animalsList.add(animals);
        }
        //System.out.println(instances.get(0));
        //System.out.println(Arrays.toString(instances.toString().split(",")));

        return animalsList;
    }
}
