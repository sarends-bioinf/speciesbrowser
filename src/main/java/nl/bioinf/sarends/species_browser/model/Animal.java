package nl.bioinf.sarends.species_browser.model;

public class Animal {

    private String scientificName;
    private String englishName;
    private String dutchName;
    private Double wingSpan;
    private String pictureLocation;
    private String name;

    public Animal(String scientificName, String englishName, String dutchName, Double wingSpan, String pictureLocation) {
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.wingSpan = wingSpan;
        this.pictureLocation = pictureLocation;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getDutchName() {
        return dutchName;
    }

    public Double getWingSpan() {
        return wingSpan;
    }

    public String getPictureLocation() {
        return pictureLocation;
    }

    public String getName(){
        String name = this.englishName.substring(0, this.englishName.indexOf(" "));
        return name.toLowerCase();
    }
}
