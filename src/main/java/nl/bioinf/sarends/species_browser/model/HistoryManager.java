package nl.bioinf.sarends.species_browser.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class HistoryManager {
    public static final int DEFAULT_MAXIMUM_SIZE = 5;
    private int maximumSize;
    private LinkedList<String> history = new LinkedList<>();

    public HistoryManager() {
        this.maximumSize = DEFAULT_MAXIMUM_SIZE;
    }

    public HistoryManager(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public void addItem(String historyItem) {
        if (history.size() == maximumSize) {
            history.remove(history.size() - 1);
        }
        history.add(0, historyItem);
    }

    public List<String> getHistoryItems() {
        return Collections.unmodifiableList(this.history);
    }

    @Override
    public String toString() {
        return history.toString();
    }

    public static void main(String[] args) {
        HistoryManager hm = new HistoryManager(4);
        hm.addItem("one");
        System.out.println("hm = " + hm);
        hm.addItem("two");
        System.out.println("hm = " + hm);
        hm.addItem("three");
        System.out.println("hm = " + hm);
        hm.addItem("four");
        System.out.println("hm = " + hm);
        hm.addItem("five");
        System.out.println("hm = " + hm);
        hm.addItem("six");
        System.out.println("hm = " + hm);

    }
}
